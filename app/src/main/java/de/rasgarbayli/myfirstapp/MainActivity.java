package de.rasgarbayli.myfirstapp;

import android.app.ActionBar;
import android.app.usage.UsageEvents;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.CalendarContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "de.rasgarbayli.myfirstapp.MESSAGE";
    static final String MY_STR_KEY = "my_string_key_for_saving_state";
    private static final String SK_MSG = "storage_key_message";

    TextView mTextView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = (TextView) findViewById(R.id.text_message);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            ActionBar actionBar = getActionBar();

            if (actionBar != null) {
                actionBar.setHomeButtonEnabled(false);
                mTextView.setText("Home button was disabled.\n");
            } else {
                mTextView.setText("ActionsBar=NULL\n");
            }

            mTextView.setText(mTextView.getText() + "Build.VERSION=" + Build.VERSION.SDK_INT);
        }

        // Test reading/writing from/into internal/external storage, sqlite database
        //opInternalStorage();
        //opExternalStorage();
        //opDatabase();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * Called in onCreate(), reads/writes from/into internal storage. For learning purposes.
     */
    private void opInternalStorage() {
        Toast.makeText(getApplicationContext(), "opInternalStorage() was called.", Toast.LENGTH_SHORT).show();
        // First save
        String filename = "myfile";
        String stringToPut = "String written into the Internal storage.";
        File file = new File(getFilesDir(), filename);
        FileOutputStream outputStream;
        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(stringToPut.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Now read the saved data
        String stringRead;
        FileInputStream inputStream;
        try {
            inputStream = openFileInput(filename);
            byte[] buffer = new byte[256];
            inputStream.read(buffer);
            stringRead = new String(buffer);
            Toast.makeText(getApplicationContext(), stringRead, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), "opInternalStorage() was completed.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Called in onCreate(), reads/writes from/into external storage. For learning purposes.
     */
    private void opExternalStorage() {
        Toast.makeText(getApplicationContext(), "opExternalStorage() was called", Toast.LENGTH_SHORT).show();
        String state = Environment.getExternalStorageState();
        if (!Environment.MEDIA_MOUNTED.equals(state)) {
            Toast.makeText(getApplicationContext(), "External Storage is not available.", Toast.LENGTH_SHORT).show();
            return;
        }
        // External storage is available, lets write into it
        String filename = "settings.txt";
        String stringToPut = "String written into the External storage.";
        // getExternalStoragePublicDirectory(sameParapemetersAsAbove) returns the public directory, which remains after uninstallation of the app.
        File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), filename);
        if (!file.mkdirs()) {
            Toast.makeText(getApplicationContext(), "Directory could not be created.", Toast.LENGTH_SHORT).show();
        }
        if ((file.getFreeSpace() / 1024) < 1) {
            // there is less than 1MB
            Toast.makeText(getApplicationContext(), "There is no enough space in the external storage.", Toast.LENGTH_SHORT).show();
            return;
        }
        // Writing into external file.
        FileOutputStream outputStream;
        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(stringToPut.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Reading from external file.
        String stringRead;
        FileInputStream inputStream;
        try {
            inputStream = openFileInput(filename);
            byte[] buffer = new byte[256];
            inputStream.read(buffer);
            stringRead = new String(buffer);
            Toast.makeText(getApplicationContext(), stringRead, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Destroying the file
        file.delete();
        Toast.makeText(getApplicationContext(), "External file was deleted.", Toast.LENGTH_SHORT).show();
        // The end
        Toast.makeText(getApplicationContext(), "opExternalStorage() was completed.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Called in onCreate(), reads/writes from/into SQLite Database. For learning purposes.
     */
    private void opDatabase() {
        // This lesson was skipped
        //Toast.makeText(getApplicationContext(), "opDatabase() was called", Toast.LENGTH_SHORT).show();

        // The end
        //Toast.makeText(getApplicationContext(), "opDatabase() was completed.", Toast.LENGTH_SHORT).show();
    }

    /**
     * Called when the user clicks the Send button
     */
    public void sendMessage(View view) {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void dispatchOwnCamera(View view) {
        Intent intent = new Intent(this, ControllingCamera.class);
        startActivity(intent);
    }

    /**
     * Called when the user clicks the save button
     */
    public void save(View view) {
        SharedPreferences.Editor editor = getPreferences(Context.MODE_PRIVATE).edit();
        editor.putString(SK_MSG, ((EditText) findViewById(R.id.edit_message)).getText().toString());
        editor.commit();
        Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
    }

    /**
     * Called when the user clicks the load button
     */
    public void load(View view) {
        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        String savedValue = preferences.getString(SK_MSG, "defaultValue");
        ((EditText) findViewById(R.id.edit_message)).setText(savedValue);
        Toast.makeText(getApplicationContext(), "Loaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        String s = "SAVED: " + ((TextView) findViewById(R.id.text_message)).getText();
        outState.putString(MY_STR_KEY, s);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            ((TextView) findViewById(R.id.text_message)).setText(savedInstanceState.getString(MY_STR_KEY));
            Toast.makeText(getApplicationContext(), "Previous state was respored.", Toast.LENGTH_SHORT).show();
        } else
            Toast.makeText(getApplicationContext(), "Previous state was not found, it is NULL", Toast.LENGTH_SHORT).show();
    }

    /**
     * Called when the user clicks the take picture button
     */
    public void takePicture(View view) {
        Intent intent = new Intent(this, camera.class);
        startActivity(intent);
    }

    /**
     * Called when the user clicks the record a video button
     */
    public void recordVideo(View view) {
        Intent intent = new Intent(this, RecordVideo.class);
        startActivity(intent);
    }

    /**
     * Called when the user clicks the call another app button
     */
    public void callAnotherApp(View view) {
        Toast.makeText(getApplicationContext(), "Calling another app.", Toast.LENGTH_SHORT).show();
        /* This calls the given number
        Uri number = Uri.parse("tel:5551234");
        Intent callIntent = new Intent(Intent.ACTION_DIAL, number);*/
        // This opens the location on google map
        Uri location = Uri.parse("geo:37.422219,-122.08364?z=14"); // lat/lon, z = zoom level for map api
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, location);
        /* This loads the webpage in a browser
        Uri webPage = Uri.parse("http://www.android.com");
        Intent webIntent = new Intent(Intent.ACTION_VIEW, webPage);*/
        /* This one creates an e-mail with attachment
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType(HTTP.PLAIN_TEXT_TYPE);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {"jon@example.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message text.");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));*/
        /* This one creates a calendar event
        Intent calendarIntent = new Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI);
        Calendar beginTime = Calendar.getInstance().set(2012, 0, 19, 7, 30);
        Calendar endTime = Calendar.getInstance().set(2012, 0, 19, 10, 30);
        calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis());
        calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis());
        calendarIntent.putExtra(CalendarContract.Events.TITLE, "Ninja class");
        calendarIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, "Secret Dojo");*/

        // Verify responsible app
        PackageManager packageManager = getPackageManager();
        if (packageManager.queryIntentActivities(mapIntent, PackageManager.MATCH_DEFAULT_ONLY).size() > 0) {
            startActivity(mapIntent);
        } else {
            Toast.makeText(getApplicationContext(), "The called app does not installed.", Toast.LENGTH_SHORT).show();
        }

        /* App chooser version
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(HTTP.PLAIN_TEXT_TYPE);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"jon@example.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Email subject");
        intent.putExtra(Intent.EXTRA_TEXT, "Email message text.");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("content://path/to/email/attachment"));
        String chooserTitle = "Send via:";
        Intent chooser = Intent.createChooser(intent, chooserTitle);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }*/
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://de.rasgarbayli.myfirstapp/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://de.rasgarbayli.myfirstapp/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}