package de.rasgarbayli.myfirstapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;
import android.widget.VideoView;

public class RecordVideo extends AppCompatActivity {
    static final int REQUEST_VIDEO_CAPTURE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_video);
        takeVideo();
    }

    private void takeVideo() {
        // Check permission for the camera
        int permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (permission == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(getApplicationContext(), "Permission is denied for camera!", Toast.LENGTH_SHORT);
            finish();
        }
        // We have a permission to call the camera app
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Toast.makeText(getApplicationContext(), "onActivityResult was called.", Toast.LENGTH_SHORT).show();
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            Uri videoUri = data.getData();
            ((VideoView) findViewById(R.id.videoViewer)).setVideoURI(videoUri);
        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "Taking video was cancelled.", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Taking video was unsuccessful.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
